﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.SceneManagement;
public class Disparador : MonoBehaviour
{
    public Rigidbody projectile;
    public float speed = 10;
    public AudioClip GunSound;
    AudioSource fuente;
    public Transform puntoDeDisparo;
    public Transform puntoDeDisparoBala;
    public GameObject padre;
    public bool couroutineStarted= false;
    private Animator animator;

    void Start(){
        fuente =  GetComponent<AudioSource>();
        animator =  padre.GetComponent<Animator>();
   }

    private void Awake() {
       
   }

    void Update()
    {
        //if(!couroutineStarted )
           // StartCoroutine( disparar() );
    
    }

    
    public IEnumerator  disparar(){
       // if ( (Input.GetButtonDown("Fire1") || (KinectManager.instance.IsAvailable && KinectManager.instance.IsFire))  ){ // && ballInPlay == false){ 
                couroutineStarted = true;

                fuente.clip = GunSound;
                fuente.Play();

                animator.CrossFadeInFixedTime("Shoot", 0.2f);
               /// animator.CrossFadeInFixedTime(Disparar.name, 0.5f);
                //animator.SetTrigger("Disparar");
                //Rigidbody instantiatedProjectile = Instantiate(projectile, transform.position, transform.rotation*Quaternion.Euler(-90,0,0));
                Rigidbody instantiatedProjectile = Instantiate(projectile, puntoDeDisparoBala.transform.position, puntoDeDisparoBala.transform.rotation);
                
                
                //Rigidbody instantiatedProjectile = Instantiate(projectile, transform.position, transform.rotation);
                instantiatedProjectile.velocity = transform.TransformDirection( Vector3.forward*30); 
                
                
                //instantiatedProjectile.velocity = transform.TransformDirection( new Vector3(0, -1, 0)*1);
                //Physics.IgnoreCollision( instantiatedProjectile.GetComponent<Collider>(), transform.root.GetComponent<Collider>() );
                
                RaycastHit hit;
                //if(Physics.Raycast(puntoDeDisparo.position, puntoDeDisparo.forward, out hit))
                if(Physics.Raycast(puntoDeDisparo.position, puntoDeDisparo.forward , out hit))
                {
                    if (hit.transform.CompareTag("Enemigo"))
                    {
                        Vida vida = hit.transform.GetComponent<Vida>();
                        if(vida == null)
                        {
                            throw new System.Exception("No se encontro el componente Vida del Enemigo");
                        }
                        else
                        {
                            vida.RecibirDanho(25);
                            //throw new System.Exception("T_T");
                        }
                    }
                }
                yield return new WaitForSeconds(0.5f);
                couroutineStarted = false;

    // }
   }

   
}
