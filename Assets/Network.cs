﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Network : Photon.PunBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        //print("hola");
        PhotonNetwork.ConnectUsingSettings("v1.0");
    
    }

    public override void OnConnectedToPhoton()
    {
        print("connected to photon");
    }

    public override void OnConnectedToMaster()
    {
        print("connected to server");
        var options = new RoomOptions();
        options.MaxPlayers = 10;
        PhotonNetwork.JoinOrCreateRoom("Test Room",options,TypedLobby.Default);
    }

    public override void OnCreatedRoom()
    {
        print("cree un cuarto");
    }

    public override void OnJoinedRoom()
    {
        print("me uni a un cuarto");
        Vector3 myVector = new Vector3(157.0f, 0.5f, 33f);
        //PhotonNetwork.Instantiate("Player66",myVector,Quaternion.identity,0);
    }
    

    // Update is called once per frame
    void Update()
    {
        
    }
}
