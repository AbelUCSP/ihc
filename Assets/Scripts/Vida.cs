﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Vida : Photon.PunBehaviour {
    public float valor = 100;
    public bool infectado = false; 

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}


    [PunRPC]
    public void RecibirDanho(float danho)
    {
        if(photonView.isMine){
            valor -= danho;
            if(valor < 0)
            {
                valor = 0;
            }
        }
    }

    void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if(stream.isWriting)
        {
            stream.SendNext(valor);
            stream.SendNext(infectado);
        }else
        {
            //Los agarro...
            valor=(float)stream.ReceiveNext();
            infectado=(bool)stream.ReceiveNext();
        }


    }
}
