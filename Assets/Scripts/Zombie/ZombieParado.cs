﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;


public enum ZombieEstado
{
    Caminar,
    Perseguir,
    Atacar,
    Muerto
}



public class ZombieParado : Photon.PunBehaviour 
{

    public GameObject militar;
    public GameObject target2;
    public float cantidad = 0.0f;
    public float distanciaDelBlanco;
    public float angle;
    public bool caminando = false;
    public bool persiguiendo = false;
    public bool estaAtacando = false;
    public bool muerto = false;
    public GameObject tipoSangre;
    private GameObject target;
    private NavMeshAgent agente;
    private Vida vida;
    private Animator animator;
    private Collider collider;
    private Vida vidaJugador;
    //private Human logicaJugador;
    private PlayerControllerPhoton logicaJugador;
    public float speed = 0.2f;
    public float daño = 25;
    public float disss = 0;

    public bool mirando = false;
    public bool mirandoAtacar = false;

    public ZombieEstado Estado;
    private void Awake()
    {
        //target = GameObject.Find("Player");
        //target = GameObject.FindGameObjectWithTag("Player");
        target = militar;
        vidaJugador = target.GetComponent<Vida>();
        if (vidaJugador == null)
        {
            throw new System.Exception("El objeto Jugador no tiene componente Vida");
        }


        //logicaJugador = target.GetComponent<Human>();
        logicaJugador = target.GetComponent<PlayerControllerPhoton>();
        if (logicaJugador == null)
        {
            throw new System.Exception("El objeto Jugador no tiene componente LogicaJugador");
        }

        agente = GetComponent<NavMeshAgent>();
        vida = GetComponent<Vida>();
        animator = GetComponent<Animator>();
        collider = GetComponent<Collider>();
    }

    private void Start()
    {
        Estado = ZombieEstado.Caminar;
    }


    private void Update()
    {

        target = cmoquieres();

        //target = GameObject.FindGameObjectWithTag("Player");
        vidaJugador = target.GetComponent<Vida>();

        //target = GameObject.Find("Adri (1)");
        EstaDefrenteAlJugador();
        RevisarVida();

        if (Estado == ZombieEstado.Caminar)
        {
            Walk();
        }
        if (Estado == ZombieEstado.Perseguir)
        {
            Perseguir();
        }
        if (Estado == ZombieEstado.Atacar)
            Atacar();

    }

    void EstaDefrenteAlJugador()
    {
        Vector3 actual = target.transform.position - transform.position;
        angle = Vector3.Angle(actual, transform.forward);
        distanciaDelBlanco = Vector3.Distance(target.transform.position, transform.position); //GLOBAL
        if (Vector3.Angle(actual, transform.forward) < 46f && distanciaDelBlanco < 9.5f)
        { // angulo entre jugador y zombie
            mirandoAtacar = true;
        }
        else
        {
            mirandoAtacar = false;
        }

        if (Vector3.Angle(actual, transform.forward) < 85f && distanciaDelBlanco < 20f)
        { // angulo entre jugador y zombie
            mirando = true;
        }
        else
        {
            mirando = false;
        }
    }


    void RevisarVida()
    {
        if (muerto) return;
        if (vida.valor <= 0)
        {
            photonView.RPC("die",PhotonTargets.All);
        }
    }

    [PunRPC]
    void die() {

        muerto = true;
        agente.isStopped = true;
        collider.enabled = false;
        animator.CrossFadeInFixedTime("Muerto", 0.3f);
        if(PhotonNetwork.isMasterClient){
            PhotonNetwork.Destroy(gameObject);
        }
        //Destroy(gameObject, 3f);
        Instantiate(tipoSangre, transform.position - new Vector3(0.1f, -1.2f, 1f), transform.rotation);
        
    }

    void Walk() //random
    {
        if (muerto) return;
        if (logicaJugador.muerto) return;
        agente.speed = speed = 0.4f;
        persiguiendo = false;
        estaAtacando = false;
        caminando = true;
        agente.SetDestination(new Vector3(Random.Range(-10.0f, 10.0f), 0, Random.Range(-10.0f, 10.0f)));


        if (mirando)
        {
            Estado = ZombieEstado.Perseguir;
            agente.speed = speed = 0.8f;
            persiguiendo = true;
            caminando = false;
        }
    }


    void Perseguir()
    {
        if (muerto) return;
        if (logicaJugador.muerto) return;

        agente.speed = speed = 0.8f;
        estaAtacando = false;
        caminando = false;
        persiguiendo = true;

        disss = distanciaDelBlanco;
        agente.destination = target.transform.position;

        if (distanciaDelBlanco <= 1.78f && mirandoAtacar)
        {
            Estado = ZombieEstado.Atacar;
        }
        else if (distanciaDelBlanco >= 10.78f)
        {
            Estado = ZombieEstado.Caminar;
            agente.speed = speed = 0.4f;
            caminando = true;
            persiguiendo = false;
        }
    }


    void Atacar()
    {
        if (muerto) return;
        if (estaAtacando) return;
        if (logicaJugador.muerto)
        {
            return;
        }

        persiguiendo = false;
        caminando = false;
        if (distanciaDelBlanco <= 1.78f && mirandoAtacar)
        {
            vidaJugador.RecibirDanho(daño);
            agente.speed = speed = 0f;
            estaAtacando = true;
            animator.SetTrigger("DebeAtacar");
            Invoke("ReiniciarAtaque", 1.5f);
        }
        else
        {
            Estado = ZombieEstado.Caminar;
            estaAtacando = false;
        }
    }

    void ReiniciarAtaque()
    {
        estaAtacando = false;
        agente.speed = speed;
    }

    void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if(stream.isWriting)
        {
            stream.SendNext(muerto);
        }else
        {
            //Los agarro...
            muerto=(bool)stream.ReceiveNext();
        }

    }

    GameObject cmoquieres()
    {
        GameObject[] gameObjects;
        gameObjects = GameObject.FindGameObjectsWithTag("Player");
        GameObject closest = null;
        float distance = Mathf.Infinity;
        Vector3 position = transform.position;

        if (gameObjects.Length == 0)
        {
            Debug.Log("No game objects are tagged with 'Player'");
            return null;
        }
        else
        {
            cantidad = gameObjects.Length;
            foreach (GameObject go in gameObjects)
            {
                Vector3 diff = go.transform.position - position;
                float curDistance = diff.sqrMagnitude;
                if (curDistance < distance)
                {
                    closest = go;
                    distance = curDistance;
                }
            }
            return closest;
        }
    }

}
