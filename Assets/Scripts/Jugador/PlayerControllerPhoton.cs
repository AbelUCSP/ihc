﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;
public class PlayerControllerPhoton : Photon.PunBehaviour{
    public bool isFIREEE;
    public float XX;
    public float YY;
    public float ZZ;
    public string name ;
    public GameObject Minimapa;
    public GameObject Camara;
    public Transform padre;
    public bool dispando = false;
    public HumanEstado EstadoHuman;
    public bool caminarndo = false;
    public bool muerto = false;
    public Vida vida;
    public float Avanzar = 0f;
    public float VelocidadAvanzar = 0.15f;
    public Animator animator;
    

    //public Player ScriptPlayer;
    public Human ScriptPlayer;



    /* Variables Rotacion
    
     */
    float maxRotationX=8f;
    float minRotationX=330f;
    float landmark=200f;
    public float HorizontalSpeed = 2;
    public float VerticalSpeed = 2;
    private float currentRotationX;
    public float RotadorPistola;
    public float H;
    public float V;


    private void Awake() {
        //gameObject.name = "Player";
        EstadoHuman = HumanEstado.none;
       // Camara.SetActive(true);
    }

    void Start()
    {
        vida = GetComponent<Vida>();
        animator = GetComponent<Animator>();
        
        Minimapa  = GameObject.FindGameObjectWithTag("Minimapa");
        name = transform.gameObject.tag;
                   
                   
        if(!photonView.isMine)
        {
            Destroy(this);
            
        }
        else{
            Camara.SetActive(true);


            if(transform.gameObject.tag == "Player")
                Minimapa.SetActive(false);
            
            if(transform.gameObject.tag == "EnemyM")
                Minimapa.SetActive(true);

                
        }

        
        
    }


    void Update()
    {
      
        //Minimapa  = GameObject.FindGameObjectWithTag("Minimapa");

        //name = transform.gameObject.tag;

        
        //XX = 

        animator.ResetTrigger("Caminando");
        animator.ResetTrigger("Shoot");
        ///RevisarVida();
        photonView.RPC("RevisarVida",PhotonTargets.All);   

        //Rotación...
        if (KinectManager.instance.IsAvailable)
        {    
            RotadorPistola = KinectManager.instance.RotationPistola;
            currentRotationX = this.transform.eulerAngles.x;
    
            if (currentRotationX> maxRotationX && currentRotationX< minRotationX)
            {
                if(currentRotationX< landmark)
                    ScriptPlayer.RotadorX(0.07f);
                else
                    ScriptPlayer.RotadorX(-0.07f);
            }
                    
            else
                ScriptPlayer.RotadorX(RotadorPistola);
          
        }
        else if(VerticalSpeed*Input.GetAxis("Mouse Y") != 0f)
        {
            V = ( VerticalSpeed*Input.GetAxis("Mouse Y") ); 
            currentRotationX = padre.eulerAngles.x;
            float variationRotation=currentRotationX+V;
            if (currentRotationX> maxRotationX && currentRotationX< minRotationX)
            {
                if(currentRotationX< landmark)
                    ScriptPlayer.RotadorX(-0.07f);
                else
                    ScriptPlayer.RotadorX(0.07f);
            }
                    
            else
                ScriptPlayer.RotadorX(-V);
        }///en rotacion X


        if( HorizontalSpeed*Input.GetAxis("Mouse X") != 0f)
            ScriptPlayer.RotadorY(HorizontalSpeed*Input.GetAxis("Mouse X"));
        //else if( Camara.transform.rotation != 0f)
        else
            transform.rotation = Camara.transform.rotation;

        ///en rotacion Y



        //DISPARAR...   
        if ( (Input.GetButtonDown("Fire1") || (KinectManager.instance.IsAvailable && KinectManager.instance.IsFire))  ){ 
            isFIREEE = KinectManager.instance.IsFire;
            //disparar
            if(! ScriptPlayer.couroutineStarted ){
                //StartCoroutine( ScriptPlayer.disparar() );
                photonView.RPC("Atacar",PhotonTargets.All);
                
            }
        }//end DISPARAR



        //CAMINAR...
        if ( (KinectManager.instance.IsAvailable && KinectManager.instance.avanzar != 0f) )
        {
            //transform.Translate(Vector3.forward * Avanzar); //back    down    forward left    up
            Avanzar = KinectManager.instance.avanzar;
            ///ScriptPlayer.caminar(Avanzar);
            photonView.RPC("caminar",PhotonTargets.All,Avanzar);
        }
        else if (Input.GetAxis("Vertical") !=0f )
        {
            //Avanzar =  (Input.GetAxis("Vertical") * VelocidadAvanzar);
            Avanzar =  (Input.GetAxis("Vertical") * VelocidadAvanzar);
            ///ScriptPlayer.caminar(Avanzar);
            photonView.RPC("caminar",PhotonTargets.All,Avanzar);
            caminarndo = true;
            EstadoHuman = HumanEstado.Caminar;
        }///END CAMINAR
        
/*
        if(Input.GetKeyDown("up")){
            photonView.RPC("mutar",PhotonTargets.All);
            //ScriptPlayer.mutar();

        }*/
    }///END UPDATE

    [PunRPC]
    public void semaforo()
    {
        StartCoroutine( ScriptPlayer.Atacar() );
        print("AVATAAAAAAR DISPARANDO");
    }


    [PunRPC]
    void RevisarVida()
    {
        if (muerto){return;}
        if(vida.valor <= 0)
        {
            muerto = true;
            if(vida.infectado)
                photonView.RPC("mutar",PhotonTargets.All);
            else
                photonView.RPC("muerto",PhotonTargets.All);
             
        }
    }


}
