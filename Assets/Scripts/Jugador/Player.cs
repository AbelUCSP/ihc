﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Player : Human{
    


    /* Variables Disparar
     */
    public Rigidbody projectile;
    public float speed = 10; //no use
    public AudioClip GunSound;
    public AudioClip sound_death;
    AudioSource fuente;
    public Transform Disparador; //Ray
    public Transform PuntoDisparo; //bala
    public GameObject padre; //no use
    //public bool couroutineStarted = false; //HUMANS
    private Animator animator;


    /* Variables Rotacion
     */
    public Transform Rotador;
    private Transform Pistola;
    


    /* Variables Caminar

     */


    void Start()
    {
        fuente =  GetComponent<AudioSource>();
        animator = GetComponent<Animator>();
        Pistola = Rotador.GetComponent<Transform>();


       

    }



    public override void RotadorX(float V){
        Pistola.Rotate(V,0,0);

    }
    
    public override void RotadorY(float H){
        this.transform.Rotate(0,H,0);
    }


    [PunRPC]
    public override IEnumerator  Atacar(){
        couroutineStarted = true;
        fuente.clip = GunSound;
        
        fuente.Play();

        animator.CrossFadeInFixedTime("Shoot", 0.2f);
        //Rigidbody instantiatedProjectile = Instantiate(projectile, PuntoDisparo.transform.position, PuntoDisparo.transform.rotation);
        //instantiatedProjectile.velocity = transform.TransformDirection( Vector3.forward*30); 
        //Physics.IgnoreCollision( instantiatedProjectile.GetComponent<Collider>(), transform.root.GetComponent<Collider>() );
     
        RaycastHit hit;
        if(Physics.Raycast(Disparador.position, Disparador.forward , out hit))
        {
            if (hit.transform.CompareTag("Enemigo"))
            {
                PhotonView vida = hit.transform.GetComponent<PhotonView>();
                if(vida == null)
                    throw new System.Exception("No se encontro el componente Vida del Enemigo");
                else{
                    vida.RPC("RecibirDanho",PhotonTargets.All,25f);
                      //throw new System.Exception("T_T");
                    }
            }
            else if (hit.transform.CompareTag("EnemyM"))
            {
                PhotonView vida = hit.transform.GetComponent<PhotonView>();
                if(vida == null)
                    throw new System.Exception("No se encontro el componente Vida del Enemigo");
                else{
                    vida.RPC("RecibirDanho",PhotonTargets.All,15f);
                      //throw new System.Exception("T_T");
                    }
            }

        }
        yield return new WaitForSeconds(0.5f);
        couroutineStarted = false;
    }//end disparar



    [PunRPC]
    public override void caminar(float Avanzar){
        transform.Translate(Vector3.forward * Avanzar);
        animator.SetTrigger("Caminando");
    }

    [PunRPC]
    public override void mutar(){
        
        //Instantiate(Objetos[0], transform.position, transform.rotation);
        
        //Destroy(gameObject);
        if(photonView.isMine)
        {
            PhotonNetwork.Instantiate("ZombieMaster",transform.position,transform.rotation, 0);
            PhotonNetwork.Destroy(gameObject);
        }
    }

    public override void destruir()
    {
        PhotonNetwork.Destroy(gameObject);

    }

    [PunRPC]
    public override void muerto(){
        
        //couroutineStarted2 = true;
        fuente.clip = sound_death;
        fuente.Play();
        animator.SetTrigger("Death");
        //yield return new WaitForSeconds(0.5f);
        //couroutineStarted2 = false;
        
        Invoke("destruir",5f);
    
    }
}
