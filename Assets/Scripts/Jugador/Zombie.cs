﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Zombie : Human
{
    /* Variables Disparar
     */
    //public Rigidbody projectile;
    //public float speed = 10; //no use
    public AudioClip GunSound;
    AudioSource fuente;
    public AudioClip deathz;
    public Transform Disparador; //Ray
    //public Transform PuntoDisparo; //bala
    //public GameObject padre; //no use
    //public bool couroutineStarted = false; //HUMANS
    private Animator animator;
    //public Transform Rotador;

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        fuente =  GetComponent<AudioSource>();
        animator = GetComponent<Animator>();
    }

    public override void RotadorX(float V){
        //Pistola.Rotate(V,0,0);

    }
    
    public override void RotadorY(float H){
        this.transform.Rotate(0,H,0);
    }


    [PunRPC]
    public override IEnumerator  Atacar(){
        couroutineStarted = true;
        fuente.clip = GunSound;
        fuente.Play();

        animator.CrossFadeInFixedTime("Shoot", 0.2f);

        RaycastHit hit;
        if(Physics.Raycast(Disparador.position, Disparador.forward , out hit))
        {
            //if (hit.transform.CompareTag("Enemigo"))
            if (hit.transform.CompareTag("Player"))
            {
                PhotonView p = hit.transform.GetComponent<PhotonView>();
                Vida vida = hit.transform.GetComponent<Vida>();
                vida.infectado = true;
                p.RPC("RecibirDanho",PhotonTargets.All,25f);
                //vida.RecibirDanho(25);
                      
            }
        }

        yield return new WaitForSeconds(0.5f);
        couroutineStarted = false;
    }//end disparar



    [PunRPC]
    public override void caminar(float Avanzar){
        transform.Translate(Vector3.forward * Avanzar);
        animator.SetTrigger("Caminando");
    }

    public override void mutar(){
        
        Instantiate(Objetos[1], transform.position, transform.rotation);
        
        Destroy(gameObject);


    }
    
    public override void destruir()
    {
        PhotonNetwork.Destroy(gameObject);

    }

    [PunRPC]
    public override void  muerto(){
        
        
        //couroutineStarted2 = true;
        fuente.clip = deathz;
        fuente.Play();
        animator.SetTrigger("Death");
        //yield return new WaitForSeconds(3);
        Invoke("destruir",5f);

        //yield return new WaitForSeconds(0.2f);
        //couroutineStarted2 = false;
    }

}
