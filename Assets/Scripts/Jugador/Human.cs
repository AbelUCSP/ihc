﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public enum HumanEstado {
    none,
    Caminar,
    Correr,
    Disparar
}

public abstract class Human : Photon.PunBehaviour{
//public class Player : Photon.PunBehaviour{


    /* Variables Disparar
     
    public Rigidbody projectile;
    public float speed = 10; //no use
    public AudioClip GunSound;
    AudioSource fuente;
    public Transform Disparador; //Ray
    public Transform PuntoDisparo; //bala
    public GameObject padre; //no use
    public bool couroutineStarted = false;
    private Animator animator;


    /* Variables Rotacion
     
    public Transform Rotador;
    private Transform Pistola;
    
///////////////

    */
    public bool couroutineStarted = false;
    public bool couroutineStarted2 = false;
    
    public GameObject[] Objetos;


    public abstract void RotadorX(float V);
    public abstract void RotadorY(float H);
    [PunRPC]
    public abstract IEnumerator  Atacar();
    public abstract void caminar(float Avanzar);
    public abstract void mutar();



    public abstract void destruir();
    public abstract void  muerto();

    

}
