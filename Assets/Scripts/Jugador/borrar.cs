public abstract class GeeksForGeeks { 
  
    public void uno(){
        //....
    }
    // abstract method 'gfg()' 
    public abstract void gfg();
      
} 
  

public class Geek1 : GeeksForGeeks 
{ 
  
    // abstract method 'gfg()'  
    // declare here with  
    // 'override' keyword 

    public void uno(){
        base.uno();
        //......
    }

    public override void gfg() 
    { 
        //........
    } 
} 


public abstract class ShapesClass {

   protected float x, y;

   protected void Draw(){

       //aqui codigo

    } 

  abstract public float Area(); 

}



public class Square : ShapesClass { 

    public override float Area() {

        base.Draw(); 
        return x * y; 
    } 

}