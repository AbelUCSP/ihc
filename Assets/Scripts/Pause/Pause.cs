﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pause : Photon.PunBehaviour
{
    
    public bool paused = false;
   
   

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Space)){
            photonView.RequestOwnership();
            if (paused)
                Time.timeScale = 1;
            else
                Time.timeScale = 0;

            paused = !paused;
        }
    }

    void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if(stream.isWriting)
        {
            stream.SendNext(paused);
            stream.SendNext(Time.timeScale);
        }else
        {
            //Los agarro...
            paused=(bool)stream.ReceiveNext();
            Time.timeScale = (float)stream.ReceiveNext();
        }

    }


 

}
