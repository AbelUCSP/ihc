﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class LogicaJugador : MonoBehaviour {
    public Vida vida;
    public bool muerto = false;

	void Start () {
        //gameObject.name = "Player";
        vida = GetComponent<Vida>();
	}
	
	void Update () {
        RevisarVida();
	}

    void RevisarVida()
    {
        if (muerto){return;}
        if(vida.valor <= 0)
        {
            muerto = true;
            Invoke("ReiniciarJuego", 4f);
        }
    }

    void ReiniciarJuego()
    {
        //SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        SceneManager.LoadScene(0);
    }

   
}

