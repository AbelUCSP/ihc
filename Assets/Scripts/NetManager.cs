﻿ using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NetManager : Photon.PunBehaviour
{
    
    public DATA dataaa;
    
    void Start()
    {
        PhotonNetwork.ConnectUsingSettings("v1.0");
        print("sup");
    }

    public override void OnConnectedToPhoton()
    {
        print("connected to photon");
    }

    public override void OnConnectedToMaster()
    {
        print("connected to server");
        var options = new RoomOptions();
        options.MaxPlayers = 10;
        PhotonNetwork.JoinOrCreateRoom("Test Room",options,TypedLobby.Default);
    }

    public override void OnCreatedRoom()
    {
        print("cree un cuarto");
        for(int i=10; i<15; i++)
            PhotonNetwork.InstantiateSceneObject("Zombie V",new Vector3(132.0f+i,0.5f,30f+i),Quaternion.identity, 0,null);

        
        PhotonNetwork.InstantiateSceneObject("GameManager",new Vector3(132.0f,7.5f,30f),Quaternion.identity, 0,null);
    }

    public override void OnJoinedRoom()
    {
        print("me uni a un cuarto");
        Vector3 myVector = new Vector3(152.0f, 0.5f, 30f);
        
        
        if( DATA.dataC == 0)
            PhotonNetwork.Instantiate("Adri",myVector,Quaternion.identity, 0);
        else
            PhotonNetwork.Instantiate("ZombieMaster",myVector,Quaternion.identity, 0);

    
    
    }
    

    // Update is called once per frame
    void Update()
    {
        
    }
}
